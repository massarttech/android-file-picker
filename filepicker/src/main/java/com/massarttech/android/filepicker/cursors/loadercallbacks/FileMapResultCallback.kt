package com.massarttech.android.filepicker.cursors.loadercallbacks

import com.massarttech.android.filepicker.models.Document
import com.massarttech.android.filepicker.models.FileType

/**
 * Created by gabriel on 10/2/17.
 */

interface FileMapResultCallback {
    fun onResultCallback(files: Map<FileType, List<Document>>)
}

