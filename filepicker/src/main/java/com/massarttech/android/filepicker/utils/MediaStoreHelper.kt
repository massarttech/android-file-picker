package com.massarttech.android.filepicker.utils

import android.content.ContentResolver
import android.os.Bundle
import com.massarttech.android.filepicker.cursors.DocScannerTask
import com.massarttech.android.filepicker.cursors.PhotoScannerTask
import com.massarttech.android.filepicker.cursors.loadercallbacks.FileMapResultCallback
import com.massarttech.android.filepicker.cursors.loadercallbacks.FileResultCallback
import com.massarttech.android.filepicker.models.Document
import com.massarttech.android.filepicker.models.FileType
import com.massarttech.android.filepicker.models.PhotoDirectory
import java.util.*

object MediaStoreHelper {

    fun getDirs(
        contentResolver: ContentResolver,
        args: Bundle,
        resultCallback: FileResultCallback<PhotoDirectory>
    ) {
        PhotoScannerTask(contentResolver, args, resultCallback).execute()
    }

    fun getDocs(
        contentResolver: ContentResolver,
        fileTypes: List<FileType>,
        comparator: Comparator<Document>?,
        fileResultCallback: FileMapResultCallback
    ) {
        DocScannerTask(contentResolver, fileTypes, comparator, fileResultCallback).execute()
    }
}