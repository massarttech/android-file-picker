package com.massarttech.android.filepicker.fragments

interface PhotoPickerFragmentListener {
    fun onItemSelected()
}