package com.massarttech.android.filepicker

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.IntegerRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.massarttech.android.filepicker.models.FileType
import com.massarttech.android.filepicker.models.sort.SortingTypes
import java.util.*

/**
 * Created by droidNinja on 29/07/16.
 */
class FilePickerBuilder {

    private val mPickerOptionsBundle: Bundle = Bundle()

    fun setMaxCount(maxCount: Int): FilePickerBuilder {
        PickerManager.setMaxCount(maxCount)
        return this
    }

    fun setActivityTheme(theme: Int): FilePickerBuilder {
        PickerManager.theme = theme
        return this
    }

    fun setActivityTitle(title: String): FilePickerBuilder {
        PickerManager.title = title
        return this
    }

    fun setSelectedFiles(selectedPhotos: ArrayList<String>): FilePickerBuilder {
        mPickerOptionsBundle.putStringArrayList(FilePickerConst.KEY_SELECTED_MEDIA, selectedPhotos)
        return this
    }

    fun enableVideoPicker(status: Boolean): FilePickerBuilder {
        PickerManager.setShowVideos(status)
        return this
    }

    fun enableImagePicker(status: Boolean): FilePickerBuilder {
        PickerManager.setShowImages(status)
        return this
    }

    fun enableSelectAll(status: Boolean): FilePickerBuilder {
        PickerManager.enableSelectAll(status)
        return this
    }

    fun setCameraPlaceholder(@DrawableRes drawable: Int): FilePickerBuilder {
        PickerManager.cameraDrawable = drawable
        return this
    }

    fun showGifs(status: Boolean): FilePickerBuilder {
        PickerManager.isShowGif = status
        return this
    }

    fun showFolderView(status: Boolean): FilePickerBuilder {
        PickerManager.isShowFolderView = status
        return this
    }

    fun enableDocSupport(status: Boolean): FilePickerBuilder {
        PickerManager.isDocSupport = status
        return this
    }

    fun enableCameraSupport(status: Boolean): FilePickerBuilder {
        PickerManager.isEnableCamera = status
        return this
    }


    fun withOrientation(@IntegerRes orientation: Int): FilePickerBuilder {
        PickerManager.orientation = orientation
        return this
    }

    fun addFileSupport(
        title: String, extensions: Array<String>,
        @DrawableRes drawable: Int
    ): FilePickerBuilder {
        PickerManager.addFileType(FileType(title, extensions, drawable))
        return this
    }

    fun addFileSupport(title: String, extensions: Array<String>): FilePickerBuilder {
        PickerManager.addFileType(FileType(title, extensions, 0))
        return this
    }

    fun sortDocumentsBy(type: SortingTypes): FilePickerBuilder {
        PickerManager.sortingType = type
        return this
    }

    fun pickPhoto(context: Activity) {
        if (hasPermissions(context)) {
            mPickerOptionsBundle.putInt(
                FilePickerConst.EXTRA_PICKER_TYPE,
                FilePickerConst.MEDIA_PICKER
            )
            start(context, FilePickerConst.REQUEST_CODE_PHOTO)
        } else askPermissions(context)
    }

    fun pickPhoto(fragment: Fragment) {
        fragment.activity?.let {
            if (hasPermissions(it)) {
                mPickerOptionsBundle.putInt(
                    FilePickerConst.EXTRA_PICKER_TYPE,
                    FilePickerConst.MEDIA_PICKER
                )
                start(fragment, FilePickerConst.REQUEST_CODE_PHOTO)
            } else askPermissions(it)
        }

    }

    fun pickFile(context: Activity) {
        if (hasPermissions(context)) {
            mPickerOptionsBundle.putInt(
                FilePickerConst.EXTRA_PICKER_TYPE,
                FilePickerConst.DOC_PICKER
            )
            start(context, FilePickerConst.REQUEST_CODE_DOC)
        } else askPermissions(context)

    }

    fun pickFile(fragment: Fragment) {
        fragment.activity?.let {
            if (hasPermissions(it)) {
                mPickerOptionsBundle.putInt(
                    FilePickerConst.EXTRA_PICKER_TYPE,
                    FilePickerConst.DOC_PICKER
                )
                start(fragment, FilePickerConst.REQUEST_CODE_DOC)
            } else askPermissions(it)
        }

    }

    fun pickPhoto(context: Activity, requestCode: Int) {
        if (hasPermissions(context)) {
            mPickerOptionsBundle.putInt(
                FilePickerConst.EXTRA_PICKER_TYPE,
                FilePickerConst.MEDIA_PICKER
            )
            start(context, requestCode)
        } else askPermissions(context)

    }

    fun pickPhoto(fragment: Fragment, requestCode: Int) {
        fragment.activity?.let {
            if (hasPermissions(it)) {
                mPickerOptionsBundle.putInt(
                    FilePickerConst.EXTRA_PICKER_TYPE,
                    FilePickerConst.MEDIA_PICKER
                )
                start(fragment, requestCode)
            } else askPermissions(it)
        }
    }

    fun pickFile(context: Activity, requestCode: Int) {
        if (hasPermissions(context)) {
            mPickerOptionsBundle.putInt(
                FilePickerConst.EXTRA_PICKER_TYPE,
                FilePickerConst.DOC_PICKER
            )
            start(context, requestCode)
        } else askPermissions(context)
    }

    fun pickFile(fragment: Fragment, requestCode: Int) {
        fragment.activity?.let {
            if (hasPermissions(it)) {
                mPickerOptionsBundle.putInt(
                    FilePickerConst.EXTRA_PICKER_TYPE,
                    FilePickerConst.DOC_PICKER
                )
                start(fragment, requestCode)
            } else askPermissions(it)
        }

    }

    private fun start(context: Activity, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    FilePickerConst.PERMISSIONS_FILE_PICKER
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(
                    context,
                    context.resources.getString(R.string.permission_filepicker_rationale),
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
        }
        PickerManager.providerAuthorities =
            context.applicationContext.packageName + ".com.massarttech.android.filepicker.provider"

        val intent = Intent(context, FilePickerActivity::class.java)
        intent.putExtras(mPickerOptionsBundle)

        context.startActivityForResult(intent, requestCode)
    }

    private fun start(fragment: Fragment, requestCode: Int) {
        fragment.context?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        it,
                        FilePickerConst.PERMISSIONS_FILE_PICKER
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(
                        fragment.context, it
                            .resources
                            .getString(R.string.permission_filepicker_rationale), Toast.LENGTH_SHORT
                    ).show()
                    return
                }
            }

            PickerManager.providerAuthorities =
                it.applicationContext?.packageName + ".com.massarttech.android.filepicker.provider"

            val intent = Intent(fragment.activity, FilePickerActivity::class.java)
            intent.putExtras(mPickerOptionsBundle)

            fragment.startActivityForResult(intent, requestCode)
        }
    }

    companion object {
        @JvmStatic
        val instance: FilePickerBuilder
            get() = FilePickerBuilder()
    }

    private fun hasPermissions(context: Context): Boolean {
        val externalStorage =
            ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
        val camera = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
        val granted = PackageManager.PERMISSION_GRANTED
        return (externalStorage == granted && camera == granted)
    }

    private fun askPermissions(context: Activity) {
        ActivityCompat.requestPermissions(
            context,
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA), 0x1
        )
    }


}
