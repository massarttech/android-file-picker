# Android File picker [![](https://jitpack.io/v/com.gitlab.massarttech/android-file-picker.svg)](https://jitpack.io/#com.gitlab.massarttech/android-file-picker)
**Android file picker.**

### Setup
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:
```
dependencies {
    implementation 'com.gitlab.massarttech:android-file-picker:1.0.0'
}
```

**Credits https://github.com/DroidNinja/Android-FilePicker**