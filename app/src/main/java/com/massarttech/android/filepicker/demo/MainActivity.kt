package com.massarttech.android.filepicker.demo

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.massarttech.android.filepicker.FilePickerBuilder
import com.massarttech.android.filepicker.FilePickerConst
import com.massarttech.android.filepicker.PickerManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var selectedPhotos = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnAddPhoto.setOnClickListener {
            FilePickerBuilder.instance
                .setMaxCount(9)
                .setSelectedFiles(selectedPhotos)
                .pickPhoto(this, FilePickerConst.REQUEST_CODE_PHOTO)
        }
    }


    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO &&
            resultCode == Activity.RESULT_OK &&
            data != null
        ) {
            selectedPhotos.clear()
            selectedPhotos.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA))
            setUpImage()
        }
    }

    private fun setUpImage() {
        if (selectedPhotos.isEmpty()) return
        val imagePath1 = selectedPhotos[0]
        imageView1.setImageURI(Uri.parse(imagePath1))
        if (selectedPhotos.size > 1) {
            val imagePath2 = selectedPhotos[selectedPhotos.lastIndex]
            imageView2.setImageURI(Uri.parse(imagePath2))
        }
    }

}
